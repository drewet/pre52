# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-10-06 13:18+01:00\n"
"PO-Revision-Date: YYYY-MM-DD HH:MM+TZ\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Text inside the <img> tag for a friend's thumbnail/avatar
#: public_html/ctrl.js
msgid "{user}'s picture"
msgstr "{user} の画像"

#. Tooltip for a message shown to the owner but
#. not yet delivered because the recipient is not online
#: public_html/ctrl.js
msgid "Waiting for {user} to come online"
msgstr "{user} がオンラインになるのを待っています"

#. Tooltip for the [x] control on a message
#. that is waiting to be delivered
#: public_html/ctrl.js
msgid "Cancel sending this message"
msgstr "このメッセージの送信をキャンセル"

#. Control to collapse the friends list to fit in
#. the vertical space of the window
#: public_html/style.css
msgid "Collapse"
msgstr "折りたたむ"

#. main html page <title></title> element
#: templates/index.html
msgid "Messenger"
msgstr "Messenger"

#. Stopped Messenger application error message overlay
#. Line 1 of 5
#: templates/index.html
msgid "Contact with Messenger interrupted."
msgstr "Messenger との連絡が中断されました"

#. Stopped Messenger application error message overlay
#. Line 2 of 5
#: templates/index.html
msgid "Please restart Messenger from the Unite panel."
msgstr "Unite パネルから Messenger を開始してください"

#. Stopped Messenger application error message overlay
#. Line 3 of 5
#: templates/index.html
msgid "Open the Unite panel"
msgstr "Unite パネルを開く"

#. Stopped Messenger application error message overlay
#. Line 4 of 5
#: templates/index.html
msgid "Right click Messenger"
msgstr "Messenger を右クリック"

#. Stopped Messenger application error message overlay
#. Line 5 of 5
#: templates/index.html
msgid "Select <strong>Start</strong>"
msgstr "<strong>開始</strong> を選択"

#. Header used in Friends area before any friends
#. are added
#: templates/index.html
msgid "Get started"
msgstr "開始"

#. Message shown while retrieving friend data
#: templates/index.html
msgid "Loading…"
msgstr "読み込み中..."

#. Header and control for showing the Help page
#: templates/index.html
msgid "Help"
msgstr "ヘルプ"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Older messages:"
msgstr "古いメッセージ："

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Today"
msgstr "今日"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Yesterday"
msgstr "昨日"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Last week"
msgstr "先週"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Last month"
msgstr "先月"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "All"
msgstr "すべて"

#. Help page section 1 of 4
#. Describes how Messenger works
#: templates/index.html
msgid ""
"Use Messenger to communicate easily with your <A href=\"http://my.opera.com/"
"{username}/friends/\" target=\"_blank\">My Opera friends</A>."
msgstr ""
"Messenger を利用すると、<A href=\"http://my.opera.com/{username}/friends/\" "
"target=\"_blank\">My Opera の友達</A> と簡単に通信を行えます"

#. Help page section 2 of 4 title
#. Describes how Messenger works
#: templates/index.html
msgid "Find friends"
msgstr "友達を特定する"

#. Help page section 3 of 4 title
#. Describes how Messenger works
#: templates/index.html
msgid "Install Messenger"
msgstr "Messenger をインストールする"

#. Help page section 3 of 4 description
#. Describes how Messenger works
#: templates/index.html
msgid ""
"To use Messenger, you need to add friends on My Opera, and they must accept "
"your friend requests. Your friends also need to have <A href=\"http://unite."
"opera.com/application/412/\" target=\"_blank\">Messenger</A> installed."
msgstr ""
"Messenger を利用するには、My Opera 上での友達を追加してください。また、その友"
"達があなたのリクエストを受け入れ、<A href=\"http://unite.opera.com/"
"application/412/\" target=\"_blank\">Messenger</A> をインストールする必要があ"
"ります。"

#. Help page section 4 of 4 title
#. Describes how Messenger works
#: templates/index.html
msgid "Get online together"
msgstr "共にオンラインになる"

#. Help page section 4 of 4 description
#. Describes how Messenger works
#: templates/index.html
msgid ""
"Your My Opera friends will be listed as offline until they have Messenger "
"running. You can send messages to friends who are offline, and they will "
"receive them when both of you are online."
msgstr ""
"My Opera の友達が Messenger を起動するまで、彼らはオフライン状態としてリスト"
"されます。オフライン状態でもメッセージを送ることはできますが、両者がオンライ"
"ンになるまでメッセージは受信されません。"

#. No javascript message
#: templates/index.html
msgid "You need JavaScript enabled to use this application."
msgstr ""
"このアプリケーションを利用するには JavaScript を有効にする必要があります"

#. Tooltip for the control to insert smilies like :-) :-D etc
#: templates/index.html
msgid "Smilies"
msgstr "絵文字"

#. Button to submit the message being composed
#: templates/index.html
msgid "Send"
msgstr "送信"

#. Header text for friends section
#: templates/index.html
msgid "Friends"
msgstr "友達"

#. Message line showing the number of friend requests the owner has
#. Singular
#: templates/index.html
msgid "You have 1 friend request »"
msgstr "1 人の友達からのリクエストがあります »"

#. Message line showing the number of friend requests the owner has
#. Two or more
#: templates/index.html
msgid "You have {count} friend requests »"
msgstr "{count} 人の友達からのリクエストがあります »"

#. Line to point the owner to My Opera to search for friends
#. Careful handling the » html element
#: templates/index.html
msgid ""
"Find your friends on <A href=\"http://my.opera.com/{username}/friends/\" "
"target=\"_blank\">my.opera.com »</A>"
msgstr ""
"<A href=\"http://my.opera.com/{username}/friends/\" target=\"_blank\">my."
"opera.com »</A> で友達を特定する"

#. Header text in the friends sidebar who have Messenger
#. running currently, and are available to chat
#: templates/index.html
msgid "Online"
msgstr "オンライン"

#. Message in Friends area when all friends are Offline
#: templates/index.html
msgid "All your My Opera friends are offline."
msgstr "My Opera の友達はすべてオフライン状態です"

#. Header text in the friends sidebar who do not
#. have Messenger running currently, so they are
#. not available to chat
#: templates/index.html
msgid "Offline"
msgstr "オフライン"

#. Message in Friends area when all friends are Online
#: templates/index.html
msgid "All your My Opera friends are online."
msgstr "My Opera の友達はすべてオンライン状態です"

#. Message in Friends area when the owner has no friends
#: templates/index.html
msgid ""
"You have not yet added any My Opera friends.<BR>Find your friends on <A href="
"\"http://my.opera.com/{username}/friends/\">my.opera.com »</A>"
msgstr ""
"My Opera の友達をまだ追加していません。<BR><A href=\"http://my.opera.com/"
"{username}/friends/\">my.opera.com »</A> で友達を特定してください。"

#. Welcome message to owner, first part
#. Example "Hi kmaage"
#: templates/index.html
msgid "Hi <SPAN id=\"username\">{username}</SPAN>"
msgstr "こんにちは、<SPAN id=\"username\">{username}</SPAN> さん"

#. Message shown when no friends could be retrieved
#. from my opera. Part 2 of 5
#. Control to attempt the import again
#: templates/index.html
msgid "Retry…"
msgstr "再試行..."

#. Message shown when no friends could be retrieved
#. from my opera. Part 3 of 5
#: templates/index.html
msgid "You do not seem to have added any My Opera friends."
msgstr "My Opera の友達を追加していないようです"

#. Message shown when no friends could be retrieved
#. from my opera. Part 4 of 5
#: templates/index.html
msgid ""
"Invite your friends to join My Opera by <A href=\"http://my.opera.com/"
"{username}/friends/invite/\" target=\"_blank\">sending them e-mail</A>."
msgstr ""
"<A href=\"http://my.opera.com/{username}/friends/invite/\" target=\"_blank\">"
"メールを送信</A> で、My Opera を友達に紹介することができます"

#. Message shown when no friends could be retrieved
#. from my opera. Part 5 of 5
#: templates/index.html
msgid "No friends were imported from My Opera."
msgstr "My Opera から友達がインポートされませんでした"

#. Control for expanding the list of friends to show them all
#: templates/index.html
msgid "Show All"
msgstr "すべて表示"

#. Message to visitors who lack Messenger
#. Part 1 of 3
#: templates/index_noaccess.html templates/index_noaccess_opera.html
#: templates/index_noaccess_unite.html
msgid ""
"{username} is using <A href=\"http://unite.opera.com/application/412/"
"\">Messenger</A> on Opera Unite."
msgstr ""
"{username} は Opera Unite <A href=\"http://unite.opera.com/application/412/"
"\">Messenger</A> を利用中です"

#. Message to visitors who have neither Opera nor Unite
#. Part 2 of 3
#: templates/index_noaccess.html
msgid ""
"Interested in Opera Unite? Opera includes built-in applications that make it "
"easy to share data and communicate directly with your friends. <A href="
"\"unite.opera.com\">Learn more</A>"
msgstr ""
"Opera Unite にご興味がおありですか？Opera には、友達とのデータ共有や通信を直"
"接行えるアプリケーションが搭載されています。<A href=\"unite.opera.com\">詳細"
"</A>"

#. Message to visitors who have Opera, but not Unite
#. Part 2 of 3
#: templates/index_noaccess_opera.html
msgid "Already using Opera? Upgrade to Opera 10.10 and Messenger is included."
msgstr ""
"すでに Opera をご利用されていますか？Opera 10.10 にアップグレードすると、"
"Messenger の機能が含まれています。"

#. Message to visitors who have Opera Unite, but not Messenger
#. Part 2a of 3
#. Alt text for screenshot of how to start Messenger
#: templates/index_noaccess_unite.html
msgid "Instructions for launching Messenger"
msgstr "Messenger を起動するには"

#. Message to visitors who have Opera Unite, but not Messenger
#. Part 2b of 3
#: templates/index_noaccess_unite.html
msgid ""
"Already using Opera Unite? Use Messenger to exchange messages with "
"{username}. <A href=\"http://my.opera.com/{username}/addfriend.pl\">Become "
"friends »</A>"
msgstr ""
"すでに Opera Unite をご利用されていますか？Messenger を利用して {username} と"
"メッセージを交換することができます。<A href=\"http://my.opera.com/{username}/"
"addfriend.pl\">友達になる »</A>"

#. Message to visitors who lack Messenger
#. Part 3 of 3
#: templates/index_noaccess.html templates/index_noaccess_opera.html
#: templates/index_noaccess_unite.html
msgid ""
"<A href=\"http://unite.opera.com/application/412/\">Tell me more about "
"Messenger</A>."
msgstr ""
"<A href=\"http://unite.opera.com/application/412/\">Messenger についての詳細"
"</A>"

#. Status line showing the number of unread messages
#. the owner has. Singular
#: PresenceChat.js
msgid "You have 1 new message"
msgstr "1 件の新しいメッセージがあります"

#. Status line showing the number of unread messages
#. the owner has. More than 1
#: PresenceChat.js
msgid "You have {count} new messages"
msgstr "{count} 件の新しいメッセージがあります"

#. Status line showing the number of unread messages
#. the owner has. None.
#: PresenceChat.js
msgid "You have no new messages"
msgstr "新しいメッセージはありません"

#. Footer, common to many Opera Unite Apps
#: index.html
msgid "Powered by <A href=\"http://unite.opera.com/\">Opera Unite</A>"
msgstr "提供： <A href=\"http://unite.opera.com/\">Opera Unite</A>"
