# P.R.E.S.T.O.  W.E.B.R.E.N.D.E.R.I.N.G.  E.N.G.I.N.E:  O.P.E.R.A.  1.2.1.5.


---

## build on linux

1. apply `fix_flower_build.patch` from root of repository
2. place curl sources to `modules`
3. `./flower -v --without-kde4`

On 64-bit systems, you may need build plugin-wrapper manually.

See `flower` output with `-j1` for additional info

Original patch borrowed from http://paste.fedoraproject.org/526781/32598714/